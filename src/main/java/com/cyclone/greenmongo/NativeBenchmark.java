package com.cyclone.greenmongo;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.*;
import static java.util.Arrays.asList;

@State(Scope.Thread)
public class NativeBenchmark {
    Collection collection = GreenMongo.collection("perf");
    MongoCollection<Document> mongoCollection = collection.internal();

    @Setup
    public void setUp() {
        mongoCollection.drop();
        for (int i = 0; i < 10000; i++) {
            collection.insertOne(new Document("number", i));
        }
    }

    @TearDown
    public void tearDown() {
        mongoCollection.drop();
    }

    @Benchmark
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    @Fork(1)
    public List<Document> nativeFind() {
        return mongoCollection
                .find(
                        Filters.and(
                                gt("number", 5000),
                                lte("number", 8000),
                                nin("number", asList(6010, 6020))
                        )
                )
                .projection(Projections.include("number"))
                .skip(1000)
                .limit(100)
                .into(new ArrayList<>());
    }
}
