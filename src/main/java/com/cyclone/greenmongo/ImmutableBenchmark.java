package com.cyclone.greenmongo;

import com.cyclone.greenmongo.operation.FindManyOperation;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.openjdk.jmh.annotations.*;

import java.util.List;

import static java.util.Arrays.asList;

@State(Scope.Thread)
public class ImmutableBenchmark {
    Collection collection = GreenMongo.collection("perf");
    MongoCollection<Document> mongoCollection = collection.internal();

    @Setup
    public void setUp() {
        mongoCollection.drop();
        for (int i = 0; i < 10000; i++) {
            collection.insertOne(new Document("number", i));
        }

    }

    @TearDown
    public void tearDown() {
        mongoCollection.drop();
    }

    @Benchmark
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    @Fork(1)
    public FindManyOperation findPrepare() {
        return collection
                .find()
                .many()
                .filterGreaterThan("number", 5000)
                .filterLessOrEqual("number", 8000)
                .filterNotIn("number", asList(6010, 6020))
                .select("number")
                .skip(1000)
                .limit(100);
    }

    @Benchmark
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    @Fork(1)
    public List<Document> findExecute() {
        return collection
                .find()
                .many()
                .filterGreaterThan("number", 5000)
                .filterLessOrEqual("number", 8000)
                .filterNotIn("number", asList(6010, 6020))
                .select("number")
                .skip(1000)
                .limit(100)
                .execute();
    }
}
